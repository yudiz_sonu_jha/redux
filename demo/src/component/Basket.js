import React from 'react'
import { useSelector } from 'react-redux';


const Basket = () => {
    const myOrder = useSelector((state)=> state.order) // Useselcetor we use to get the data
console.log(myOrder);
  return (
    <div>
        {
            myOrder.map((order,index)=>{
                return(
                    <div key={index}>
                    <h4> Name : {order.name}</h4>
                    <h4> Description : {order.description}</h4>
                    <h4> Price :  ${order.price}</h4>
                    <h4> {order.variants.name} : ${order.variants.price}</h4>
                    <h4>{order.extras.map((extra)=> ` ${extra.name}`)}</h4>

                    </div>
                )
            })
        }
    </div>
  )
}

export default Basket
