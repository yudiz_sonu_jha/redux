import React from 'react';
import './App.css';
import Main from './Main';
import {BrowserRouter ,  Routes , Route} from "react-router-dom";
import Basket from './component/Basket';
// import {Intl} from"react-intl";


function App() {
  return (
    <div>
      {/* <Main/> */}

      <BrowserRouter>
          <Routes>
            <Route path = "/" element = {<Main/>} exact/>
            <Route path = "/basket" element = {<Basket/>} exact/>
          </Routes>
          </BrowserRouter>

    </div>
  );
}

export default App;
